﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using crud_user.Domain.Models;

namespace crud_user.Domain.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetById(int id);
        Task<IEnumerable<User>> GetAll();
        Task<User> Add(User user);
        Task<User> Update(User user);
    }
}
