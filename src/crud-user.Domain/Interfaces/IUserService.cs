﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using crud_user.Domain.Dtos;
using crud_user.Domain.Models;

namespace crud_user.Domain.Interfaces
{
    public interface IUserService
    {
        Task<User> GetById(int id);
        Task<IEnumerable<UserListDto>> GetAll();
        Task<User> Add(CreateUserDto createUserDto);
        Task<User> Update(UpdateUserDto updateUserDto);
        Task Delete(int id);
    }
}
