﻿using System;
using System.Collections.Generic;
using System.Text;
using crud_user.Domain.Enuns;

namespace crud_user.Domain.Models
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateBirth { get; set; }
        public SchoolingEnum Schooling { get; set; }

        public User()
        {
            IsDeleted = false;
        }
    }
}
