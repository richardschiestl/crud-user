﻿using System;
using System.Collections.Generic;
using System.Text;

namespace crud_user.Domain.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
