﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using crud_user.Domain.Dtos;
using crud_user.Domain.Models;

namespace crud_user.Domain.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateUserDto, User>().ReverseMap();
            CreateMap<UpdateUserDto, User>().ReverseMap();
            CreateMap<UserListDto, User>().ReverseMap()
                .ForMember(dest => dest.SchoolingDesc, opt => opt.MapFrom(src => src.Schooling.ToString()));

        }
    }
}
