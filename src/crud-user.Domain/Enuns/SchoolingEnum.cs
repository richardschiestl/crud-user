﻿using System;
using System.Collections.Generic;
using System.Text;

namespace crud_user.Domain.Enuns
{
    public enum SchoolingEnum
    {
        Infantil = 1,
        Fundamental = 2,
        Medio = 3,
        Superior = 4
    }
}
