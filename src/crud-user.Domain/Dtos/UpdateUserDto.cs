﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace crud_user.Domain.Dtos
{
    public class UpdateUserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateBirth { get; set; }
        public int Schooling { get; set; }

    }

    public class UpdateUserDtoValidator : AbstractValidator<UpdateUserDto>
    {
        public UpdateUserDtoValidator()
        {
            RuleFor(x => x.Id).NotNull().GreaterThan(0);
            RuleFor(x => x.Name).NotEmpty().MinimumLength(2).MaximumLength(100);
            RuleFor(x => x.LastName).NotEmpty().MinimumLength(2).MaximumLength(100);
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.DateBirth.Date).NotNull().LessThanOrEqualTo(DateTime.Today.Date);
            RuleFor(x => x.Schooling).GreaterThan(0).LessThanOrEqualTo(4);
        }
    }
}
