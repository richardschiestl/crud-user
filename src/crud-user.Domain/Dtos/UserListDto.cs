﻿using System;
using System.Collections.Generic;
using System.Text;

namespace crud_user.Domain.Dtos
{
    public class UserListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateBirth { get; set; }
        public string SchoolingDesc { get; set; }
        public int Schooling { get; set; }
    }
}
