﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using crud_user.Data.Context;
using crud_user.Domain.Interfaces;
using crud_user.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace crud_user.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetById(int id)
            => await _context.Users.FindAsync(id);


        public async Task<IEnumerable<User>> GetAll()
            => await _context.Users.ToListAsync();


        public async Task<User> Add(User user)
        {
            await _context.Users.AddAsync(user);

            await _context.SaveChangesAsync();

            return user;
        }

        public async Task<User> Update(User user)
        {
            _context.Users.Update(user);

            await _context.SaveChangesAsync();

            return user;
        }
    }
}
