﻿using System;
using System.Collections.Generic;
using System.Text;
using crud_user.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace crud_user.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasQueryFilter(x => x.IsDeleted == false);
        }
    }
}
