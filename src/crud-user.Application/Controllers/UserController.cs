﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using crud_user.Domain.Dtos;
using crud_user.Domain.Interfaces;
using crud_user.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace crud_user.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("")]
        public async Task<ActionResult> Add([FromBody] CreateUserDto createUser)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.Select(x => x.Errors.Select(e => e.ErrorMessage)));

            return Ok(await _userService.Add(createUser));
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Update([FromRoute] int id, [FromBody] UpdateUserDto updateUser)
        {
            if (id != updateUser.Id)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values.Select(x => x.Errors.Select(e => e.ErrorMessage)));

            return Ok(await _userService.Update(updateUser));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult> Get([FromRoute] int id)
        {
            var user = await _userService.GetById(id);

            if (user == null) return NotFound();

            return Ok(user);
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
            => Ok(await _userService.GetAll());

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            var user = await _userService.GetById(id);

            if (user == null) return NotFound();

            await _userService.Delete(id);

            return Ok();
        }
    }
}
