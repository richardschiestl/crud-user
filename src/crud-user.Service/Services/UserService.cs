﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using crud_user.Domain.Dtos;
using crud_user.Domain.Interfaces;
using crud_user.Domain.Models;

namespace crud_user.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<User> GetById(int id)
            => await _userRepository.GetById(id);

        public async Task<IEnumerable<UserListDto>> GetAll()
            => _mapper.Map<IEnumerable<UserListDto>>(await _userRepository.GetAll());

        public async Task<User> Add(CreateUserDto createUserDto)
            => await _userRepository.Add(_mapper.Map<User>(createUserDto));

        public async Task<User> Update(UpdateUserDto updateUserDto)
            => await _userRepository.Update(_mapper.Map<User>(updateUserDto));
        
        public async Task Delete(int id)
        {
            var user = await _userRepository.GetById(id);

            user.IsDeleted = true;

            await _userRepository.Update(user);
        }
    }
}
